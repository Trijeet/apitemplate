
app_queries = {
    "create_db": '''CREATE DATABASE IF NOT EXISTS brainstem;''',
    "create_table_user":
        '''
        CREATE TABLE IF NOT EXISTS brainstem.users (
            uuid VARCHAR(36) PRIMARY KEY,
            username VARCHAR(50) NOT NULL,
            email VARCHAR(100) NOT NULL
        );
        ''',
    "create_user": '''INSERT INTO brainstem.users (uuid, username, email) VALUES (?,?,?);''',
    "read_user": '''SELECT * from brainstem.users WHERE uuid = ?;''',
    "update_user": '''UPDATE brainstem.users SET username = ?, email = ? WHERE uuid = ?;''',
    "delete_user": '''DELETE FROM brainstem.users WHERE uuid = ?;''',
}