import flask
import dotenv
import os

from .views import views
from config import configuration


def createApp(this_conf='standard'):
    app = flask.Flask(__name__)
    # app = flask.Flask(__name__, static_url_path="/app/static", static_folder='static')

    # app.config.from_object(configuration[this_conf])

    # # Change path to the desired configuration file:
    # app.static_url_path = app.config.get('STATIC_FOLDER')
    # app.static_folder = app.root_path + app.static_url_path

    dotenv.load_dotenv()
    config = {
        'host': os.getenv("DB_HOST"),
        'port': int(os.getenv("DB_PORT")),
        'user': os.getenv("DB_USER"),
        'password': os.getenv("DB_PASS"),
        'ssl': 1
    }

    app.register_blueprint(views)
    return app


def doDB():
    print("Doing db initiation.")
